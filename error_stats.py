import sys
sys.path.append("./dkifer-hhgqsize/")
import numpy
import re
import pandas as pd
"""
from collections import OrderedDict
from main_seq import setups
from synthdata import states
from synthdata import Bootstrap

def read_errs(fp, col_name='l1'):
    df = pd.read_csv(fp)
    #print('fp: ', fp)
    #print('df: \n', df)
    #df = df[:5]

    print('inference alg: ', col_name)
    mean = df[col_name].mean()
    #print('mean: ', mean)
    median = df[col_name].median()
    #print('median: ', median)
    var = df[col_name].var()
    #print('var: ', var)
    null_cnt = df[col_name].isnull().sum()
    #print("null_cnt: ", null_cnt)
    #print('mean:{0}, median:{1}, var:{2}, null:{3}'.format(mean, median, var, null_cnt))
    d = {'{0}_mean'.format(col_name): mean, '{0}_median'.format(col_name): median, '{0}_var'.format(col_name): var, '{0}_null'.format(col_name): null_cnt}
    return OrderedDict(sorted(d.items(), key=lambda t: t[0]))
def state_agg_csv():
    res_dir = './mlltrng/'
#    state_folder = 'mlltrng-state-seed20-60/'
    state_folder = 'mlltrng-state-allsetup-seed20-60/'
    level = 'state'
    final_res_df = pd.DataFrame()
#    for param in [ ('CONTnoConstr',[]), ('CONTx0', []) ]:
    for param in setups:
        print(param[0])
        #print("states:", states)
        #[Bootstrap(name='al', sizehist=array([      0, 1883791,  516696,  632291,  322941,  245326,  106771,
        # 37510,   22256])), Bootstrap(name='ak', sizehist=array([  ...

        setup_df = pd.DataFrame()
        for st in states+[Bootstrap('statesSum', [])]:
            print(st.name) #al
            fn = 'setup-{0}_level-{1}_st-{2}.csv'.format(param[0], level, st.name)
            try:
                res_df = pd.read_csv(res_dir+state_folder+fn)
            except FileNotFoundError:
                print("{0} not exist".format(fn))
                continue

            #aggregate all runs in fn
            #print("is null count\n", res_df.isnull().sum())
            # l1      0
            #l2      3
            #seed    0
            #dtype: int64
            #agg_null = res_df.isnull().sum()
            st_l1l2 = OrderedDict({'st': st.name})

            for col_name in ['l1', 'l2']:
                row = read_errs(res_dir+state_folder+fn, col_name=col_name)
                st_l1l2.update(row)

            print("update both l1 l2 : ", st_l1l2)
            setup_df = setup_df.append(st_l1l2, ignore_index=True)

        if len(setup_df) == 0:
            print("======this setup result is not ready======")
        else:
            #print("after all states result: \n", setup_df)
#           print(setup_df.columns)
            #Index(['l1_mean', 'l1_median', 'l1_null', 'l1_var', 'l2_mean', 'l2_median',

            setup_df.set_index(['st'], inplace=True)
            #print("after set index: \n", setup_df)
            #                  l1_mean  l1_median  l1_null        l1_var        l2_mean  \
            #st
            #al         100071.000000    99999.0      0.0  9.984410e+09  658381.418696

#           print("column name: ", setup_df.columns)

            setup_df.rename(columns=lambda x: '{0}_{1}'.format(param[0], x), inplace=True)
            #print("after setupname as prefix: ",  setup_df.columns)
            #Index(['CONTx0_l1_mean', 'CONTx0_l1_median',
            print("after set index: \n", setup_df)

            final_res_df = pd.concat([final_res_df, setup_df], axis=1)
    print("final all setups: \n", final_res_df)
    print("final_res_df size: ", len(final_res_df))
    final_res_df = final_res_df.round(3)
    final_res_df.to_csv('res_from_{0}.csv'.format(state_folder[:-1]), index=True)

def national_agg_csv():
    res_dir = './mlltrng/'
    national_folder = 'mlltrng-national/'
    level = 'nat'
    print(setups)#[('CONTx0', {'var_type': 'CONTINUOUS', 'starts_at': 0, 'weighted_total': None, 'ends_at': None}), ('CONTx0xend', {'var_type': 'CONTINUOUS', 'starts_at': 0, 'weighted_total': None, 'ends_at': True}),
    final_df = pd.DataFrame()
    for param in setups:
#    for param in [ ('CONTnoConstr',[]), ('CONTx0', []) ]:
        print(param[0]) #CONTx0
        fn = 'setup-{0}_level-{1}.csv'.format(param[0], level)
        try:
            res_df = pd.read_csv(res_dir+national_folder+fn)
            #print("res_df: \n", res_df)
        except FileNotFoundError:
            print("{0} not exist".format(param[0]))
            continue

        nat_l1l2 = OrderedDict({'setup': param[0]})
        for col_name in ['l1', 'l2']:
            row = read_errs(res_dir+national_folder+fn, col_name=col_name)
            print("row: ", row)
            nat_l1l2.update(row)

        print("update both l1 l2 of one setup: ", nat_l1l2)
        final_df = final_df.append(nat_l1l2, ignore_index=True)
        #print("final_df: ", final_df)
    print("====the end: final_df ======", final_df)
    final_df.set_index(['setup'], inplace=True)
    final_df = final_df.round(3)
    final_df.to_csv('res_from_{0}.csv'.format(national_folder[:-1]), index=True)
"""
def plot_err(fn):
    #python2
    import matplotlib.pyplot as plt

    res_dir = './mlltrng/'
    folder = 'seed_hist_res/'
    l1fn = fn+'l1estSubtrue'
    print res_dir+folder+l1fn
    l1_diff = numpy.loadtxt(res_dir+folder+l1fn)
    print'l1_diff: ', l1_diff


    l2fn = fn+'l2estSubtrue'
    l2_diff = numpy.loadtxt(res_dir+folder+l2fn)

    i  = numpy.arange(l1_diff.size)


    plt.subplot(211)
    plt.plot(l1_diff[:10500], label='l1 esti-true')
    plt.legend(loc='best')

    plt.subplot(212)
    plt.plot(l2_diff[:10500], label='l2 esti-true')
    plt.legend(loc='best')

    plt.tight_layout()
    plt.show()



if __name__ == '__main__':
    #read_errs('./default_l1.csv', col_name='l1')

#    print("======national level======")
#    national_agg_csv()

#    print("======state level======")
#    state_agg_csv()

#    plot_err('setup-CONTnoConstr_level-nat_seed-22_') #large err
    plot_err('setup-CONTnoConstr_level-nat_seed-23_') #small err for l1
