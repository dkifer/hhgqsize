import mechanisms
import numpy

def noisy_increasing_hist(data, budget, prng, max_size):
    """ Adds noise to the cumulative sum of a histogram except for the first
    element which is assumed to be 0 and the last element which is assumed to
    be known since it is the number of groups

    Input:
        data: a group size histogram in a numpy array
        budget: the privacy budget to use
        prng: a numpy.random.RandomState instance
        max_size: the maximum group size to use. This should be
             either known or a privacy-preserving estimate
    Output:
        hist: the noisy cumulative sum histogram

    """
    myhist = numpy.zeros(max_size)
    myhist[0:data.size]=data
    cumsum = myhist.cumsum()

    hist = numpy.zeros(cumsum.size)
    hist[0]=0.0
    hist[-1] = cumsum[-1]
    hist[1:hist.size-1] = mechanisms.geometric_mechanism(true_answer = cumsum[1:hist.size-1], budget=budget, sensitivity=1.0, prng=prng)
    return hist

def noisy_group_sizes(data, budget, prng):
    """ Adds noisy to each group (i.e. household)

    Inputs:
        data: a numpy array where data[i] is the number of groups (i.e. household) of size i
        budget: privacy budget
        prng: a numpy RandomState instance
    Outputs:
        hist: a dictionary where h[i] is the number of groups of size i
        arr_hist: a numpy array of the noisy group sizes. It is guaranteed that
             the original group sizes corresponding to them are in nondecreasing order.
    Notes:
        the noisy group sizes can be negative, but they will be integers

    """
    hist = {}
    index = 0
    print('data.sum(): ', data.sum()) #116560291 #total number of households
    arr_hist = numpy.zeros(data.sum())
    for (index, count) in enumerate(data):
        #when size is 'index', there are 'count' number of households
        #print('index: ', index) #1
        #print('count: ', count) #31204910        
        if count != 0:
            index_range = list(range(index, count + 1))
            noisy_group_size = mechanisms.geometric_mechanism(true_answer=index_range, budget=budget, sensitivity=1.0, prng=prng)
            #print('noisy_group_size: ', noisy_group_size)
            arr_hist[index:count+1] = noisy_group_size
            #print('arr_hist: ', arr_hist)
            for n in noisy_group_size:
                hist[n] = hist.get(n, 0) + 1
            #print('hist: ', hist)
    return hist, arr_hist



def analyze_noisy_groups(hist, budget, prob=0.000001):
    """ Given noisy group sizes, finds estimated max group size
    returns a histogram of estimated true group sizes so its ith entry
    is number of estimated groups of size i. The estimation uses maximum
    likelihood (or bayesian inference with uniform prior).

    Inputs:
        hist: a dictionary of noisy groups. h[i] is the number of noisy groups with size i
        budget: the privacy budget used to construct hist
        prob: the probability that we will understimate the maximum group size
    Outputs:
        mlhist: a histogram of estimated group sizes. mlhist[i] is the number of groups that
            have size i. The estimates are fractional. The size of mlhist is infered from the
            largest noisy grou size.

    """
    noisy_max = max(hist.keys())
    slack = -numpy.log(2.0 * prob)/float(budget)
    est_max = int(numpy.ceil(noisy_max + slack))
    mlhist = numpy.zeros(est_max+1) #maximum likelihood estimate of group size histogram
    # will be updated for each iteration i
    # will be equal to the probability that groups with noisy values less than or equal to i
    #   actually came from a group with size i
    left = 0.0
    # will be updated in reverse for each iteration i
    # will be equal the the probability that groups with noisy values > est_max -i
    #    actually came from a group with size est_max - i
    right = 0.0
    for gsize in hist:
        if gsize <= 0:
            left = left + hist[gsize] * mechanisms.geo_mech_prob(0-gsize,0-gsize,budget)/mechanisms.geo_mech_prob(0-gsize, est_max-gsize, budget)
    next = 0
    for i in range(mlhist.size):
        mlhist[i] = mlhist[i] + left
        next_l = hist.get(i+1,0)  * mechanisms.geo_mech_prob(0,0,budget)/mechanisms.geo_mech_prob(0-(i+1), est_max-(i+1), budget)
        left = left * numpy.exp(-budget) + next_l
        mlhist[est_max-i] = mlhist[est_max-i]+right
        next_r = hist.get(est_max-i,0)  * mechanisms.geo_mech_prob(0,0,budget)/mechanisms.geo_mech_prob(0-(est_max-i), est_max-(est_max-i), budget)
        right = (right + next_r) * numpy.exp(-budget)
    return mlhist


def combine(data, budget, prng):
    ratio = 0.5
    gsize_budget = budget * ratio
    noisy_gsize, noisy_glist = noisy_group_sizes(data, gsize_budget, prng)
    mlest = analyze_noisy_groups(noisy_gsize, gsize_budget)
    inc_budget = budget - gsize_budget
    noisy_inc_hist = noisy_increasing_hist(data, inc_budget, prng, maxsize=mlest.size)

"""
if __name__ == '__main__':
    import synthdata as syn
    import time
    budget = 1
    seed = 20
    prng = numpy.random.RandomState(seed)

#    sizehist = syn.synthdata(seed=seed, public_max_size=60)#100000)
    sizehist = numpy.array([0, 0, 3, 2])
#    sizehist = numpy.array([0, 5, 3]) #arr_hist: [ 0. -1.  1.  3.  5.  6.  0.  0.]
#    sizehist = numpy.array([0, 5, 3, 8])
#    sizehist = numpy.array([1, 5, 3]) #arr_hist:  [-2.  2.  2.  4.  3.  4.  0.  0.  0.]
#    print(sizehist)
#    noisy_cum_sum = noisy_increasing_hist(sizehist, budget, prng, len(sizehist))
#    if type(noisy_cum_sum) is list:
#        print('numpy array')
    start_time = time.time()
    _, tmp = noisy_group_sizes(sizehist, budget, prng)
    print('tmp: ', tmp)
    print("---noisy household func %s seconds ---" % (time.time() - start_time))

"""