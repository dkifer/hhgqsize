# README #

This repository is for code for estimating group size distributions using differential privacy

### Getting Started

The initial commit has 4 files

- synthdata.py, which produces synthetic data. It has two important functions 
    - synthdata(seed) produces 1 histogram to simulate the U.S. group population. The result is a histogram h with h[i] being the number of groups with i people in it.
	- statedata(seed) produces a dictionary of size histograms where the key is a state and the value is a group size histogram. note that the state histograms are not necessarily consistent with the histogram you would get from synthdata(seed)
	
- combine.py shows how to produces some differentially private noisy statistics about the data
    - noisy_increasing_hist shows how to create a cumulative sum histogram and add noise to satisfy differential privacy. No noise is added to the 0th entry of the histogram (because no groups have size 0) and no noise is added to the last entry (because the number of groups is known)
	- noisy_group_sizes shows how to add noise to every individual group size
	- you have a fixed privacy budget (e.g., 1.0). You can use it on noisy_increasing hist or noisy_group_sizes. If you want to use it on both, you need to split the privacy budget across both calls
	
- evaluate
    - after you do inference on the differentially private noisy data to estimate group sizes, you call evaluate.emd to evaluate the result. Note that the input to emd is not an increasing histogram, it is a histogram k where k[i] is the estimate of the number of groups of size i
	
- mechanisms
    - this contains code for adding differential privacy to statistics computed from data
	
### TODO

- Create L1 and L2 inference routines that take a noisy increasing histogram and find an increasing histogram that approximates it
- We need to evaluate the error of both methods, so they all need to be called multiple times (e.g., 10-40) with different seeds and the results should be aggregated. The dask.distributed python library should help doing this. The results can be saved as a CSV and the python pandas library can be used to load and analyze this data
- We need to evaluate l1 and l2 inference with the following features on/off: (1) adding constraint that the 0th element is 0, (2) adding constraint that the maximum element must match the maximum of the true increasing histogram, (3) adding constraint that the total population in the inferred histogram matches that of the true histogram
- Evaluate national level histogram first. Then see how it works for each state (compared to accuracy on the histogram that is the sum of all states).