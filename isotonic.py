from gurobipy import *

import numpy
def isotonic_l1(arr, var_type="INTEGER", cumsum=True, nonnegative=True, starts_at=None, ends_at=None, weighted_total=None):
    """ Performs L1 isotonic regression on arr

    Inputs:
            arr: 1-d array or 2-d array where row 0 is unit array row 1: are sub-unit array
            nonnegative: specifies whether returned array x must have nonnegative values
            starts_at: either None or a real value. If not None, then x[0] is constrained to equal starts_at
            ends_at: either None or a real value. If not None then X[arr.size-1] is constraint to equal ends_at
            weighted_total: either None or a real value. If not None, then

                1) sum_i  i * (x[i] - x[i-1])  = weighted_total (simplify the cancellations); if cumsum == True
                2) sum_i (x[i]) = weighted_total;  if cumsum == False
            cumsum: the input is the cumulative sum instead of the household size array
    Outputs:
            x: a nondecreasing array that minimizes L1 distance to arr
            x is supposed to be a cumulative sum. x[i] = number of households of size i or less.
    """


    print('arr: ', arr)
    joint_flag = True
    grb_var_type = GRB.INTEGER if var_type == "INTEGER" else GRB.CONTINUOUS

    #this is for pytest arr input is a list (1-d)
    #if arr is 1-d array and the weighted_total, ends_at are a value
    if (type(arr) is list) or (type(arr).__module__ == numpy.__name__ and arr.ndim == 1):
        print('transform')
        #transform to 2-d
        arr = numpy.array([arr])

        #transform to a list
        weighted_total = weighted_total if weighted_total is None else [weighted_total]
        #print('list weighted_total: ', weighted_total)
        ends_at = ends_at if ends_at is None else [ends_at]
        #print('list ends_at: ', ends_at)
        joint_flag = False

    length = arr.shape[1]
    row = arr.shape[0]
    print('row: ', row)


    m = Model("isotonic_l1")
    #var index: 0 ~ (row-1), 0 ~ (col-1)
    q = m.addVars(row, length, lb=(-GRB.INFINITY), vtype=grb_var_type) #row, col indices
    x = m.addVars(row, length, vtype=grb_var_type)

    m.setObjective(quicksum(q), GRB.MINIMIZE)


    for r_idx in range(row):
        print('r_idx: ', r_idx)
        if nonnegative:
            print("x nonnegative")
            m.addConstrs(x[r_idx, i] >= 0  for i in range(length))



        # set up the non-decreasing constraints for x
        m.addConstrs(x[r_idx, i] <= x[r_idx, i+1]  for i in range(length-1))

        # add n constraints for q_i >= arr_i - x_i and n constraints for q_i >= -arr_i + x_i
        m.addConstrs(q[r_idx, i] >= arr[r_idx, i] - x[r_idx, i] for i in range(length))
        m.addConstrs(q[r_idx, i] >= -arr[r_idx, i] + x[r_idx, i] for i in range(length))


        # total weight constraint
        if weighted_total is not None: #weighted_total is real

            if cumsum:
                print ("total popul constraint for cumsum")
                expr = (length - 1) * x[r_idx, length - 1] - quicksum([x[r_idx, j] for j in range(length - 1)]) #x[:n-1] = x[0]~x[n-2]
                m.addConstr(expr == weighted_total[r_idx])
            else:
                print ("total popul constraint for household size")
                expr = quicksum([x[r_idx, j] for j in range(length)])
                m.addConstr(expr == weighted_total[r_idx])

        if starts_at is not None:
            print ("starts_at constraint")
            m.addConstr(x[r_idx, 0] == starts_at) #every region shares the same

        if ends_at is not None:
            print ("ends_at constraint")
            m.addConstr(x[r_idx, length-1] == ends_at[r_idx]) #no key is -1



    # enforce consistency for sum of sub units == unit
    if joint_flag:
        #arr[0, ] is unit, sub units are: arr[1, ] arr[2, ]...
        for i in range(length): #size 0 ~ length-1
            m.addConstr(x[0, i] == quicksum([ x[sub, i] for sub in range(1, row) ]))



#    m.Params.OutputFlag = 0 #disable solver output
    m.optimize()

    print("Model status: ", m.status) #m.status should be 2 which is OPTIMAL
    if m.status != 2:
        print("Model not OPTIMAL - status: ", m.status)


    # collect x result
    opt_x = numpy.zeros((row, length))
    for r_idx in range(row):
        for i in range(length):
            try:
                opt_x[r_idx, i] = x[r_idx, i].x
            except AttributeError:
                print("=======x AttributeError=========")
                opt_x[r_idx, i] = numpy.nan
    #print('opt_x: ', opt_x) #[[ 4.  4.  4.  7.]]


    #to fit the pytest and other main func format

    if joint_flag:
        #return a matrix
        return opt_x
    else:
        # arr is 1-d
        return opt_x.flatten() #array([4, 4, 4, 7])


def isotonic_l2(arr, var_type="INTEGER", cumsum=True, nonnegative=True, starts_at=None, ends_at=None, weighted_total=None):
    """ Performs L2 isotonic regression on arr

    Inputs:
            arr: a 1-d numpy array
            nonnegative: specifies whether returned array x must have nonnegative values
            starts_at: either None or a real value. If not None, then x[0] is constrained to equal starts_at
            ends_at: either None or a real value. If not None then X[arr.size-1] is constraint to equal ends_at
            weighted_total: either None or a real value. If not None, then
                                     sum_i  i * (x[i] - x[i-1])  = weighted_total   (simplify the cancellations)
            cumsum: the input is the cumulative sum instead of the household size array
    Outputs:
            x: a nondecreasing array that minimizes L1 distance to arr
            x is supposed to be a cumulative sum. x[i] = number of households of size i or less.
    """


    joint_flag = True
    grb_var_type = GRB.INTEGER if var_type == "INTEGER" else GRB.CONTINUOUS

    if (type(arr) is list) or (type(arr).__module__ == numpy.__name__ and arr.ndim == 1):
        print('transform')
        #transform to 2-d
        arr = numpy.array([arr])
        weighted_total = weighted_total if weighted_total is None else [weighted_total]
        ends_at = ends_at if ends_at is None else [ends_at]
        joint_flag = False

    length = arr.shape[1]
    row = arr.shape[0]


    m = Model("isotonic_l2")

    q = m.addVars(row, length, lb=(-GRB.INFINITY), vtype=grb_var_type) #row, col indices
    x = m.addVars(row, length, vtype=grb_var_type)

    #m.setObjective(quicksum([q[i]*q[i] for i in range(length)]), GRB.MINIMIZE)
    m.setObjective(quicksum([q[r, i]*q[r, i] for r in range(row) for i in range(length)]), GRB.MINIMIZE)

    for r_idx in range(row):
        print('r_idx: ', r_idx)
        if nonnegative:
            print("x nonnegative")
            m.addConstrs(x[r_idx, i] >= 0  for i in range(length))

        # set up the non-decreasing constraints for x
        m.addConstrs(x[r_idx, i] <= x[r_idx, i+1]  for i in range(length-1))

        # add n constraints for q_i = arr_i - x_i
        m.addConstrs(q[r_idx, i] == arr[r_idx, i] - x[r_idx, i] for i in range(length))

        # total weight constraint
        if weighted_total is not None: #weighted_total is real

            if cumsum:
                print ("total popul constraint for cumsum")
                expr = (length - 1) * x[r_idx, length - 1] - quicksum([x[r_idx, j] for j in range(length - 1)]) #x[:n-1] = x[0]~x[n-2]
                m.addConstr(expr == weighted_total[r_idx])
            else:
                print ("total popul constraint for household size")
                expr = quicksum([x[r_idx, j] for j in range(length)])
                m.addConstr(expr == weighted_total[r_idx])

        if starts_at is not None:
            print ("starts_at constraint")
            m.addConstr(x[r_idx, 0] == starts_at) #every region shares the same

        if ends_at is not None:
            print ("ends_at constraint")
            m.addConstr(x[r_idx, length-1] == ends_at[r_idx]) #no key is -1



    # enforce consistency for sum of sub units == unit
    if joint_flag:
        #arr[0, ] is unit, sub units are: arr[1, ] arr[2, ]...
        for i in range(length): #size 0 ~ length-1
            m.addConstr(x[0, i] == quicksum([ x[sub, i] for sub in range(1, row) ]))



#    m.Params.OutputFlag = 0 #disable solver output
    m.optimize()

    print("Model status: ", m.status)
    if m.status != 2:
        print("Model not OPTIMAL - status: ", m.status)

    # collect x result
    opt_x = numpy.zeros((row, length))
    for r_idx in range(row):
        for i in range(length):
            try:
                opt_x[r_idx, i] = x[r_idx, i].x
            except AttributeError:
                print("=======x AttributeError=========")
                opt_x[r_idx, i] = numpy.nan


    #to fit the pytest and other main func format
    if joint_flag:
        return opt_x
    else:
        return opt_x.flatten()




if __name__ == '__main__':
    #arr = [0, 3, 3, 5]
#    arr =  [6, 4, 3, 7]

    ##2-d array
#    arr = numpy.array([[6, 4, 3, 7]]) #shape (1, 4)
#    result = isotonic_l1(arr, var_type="CONTINUOUS", starts_at=0, ends_at=[5], weighted_total=[11]) #

    ##1-d array
#    arr =  [6, 4, 3, 7]
#    result = isotonic_l1(arr, var_type="CONTINUOUS", starts_at=0, ends_at=5, weighted_total=11) #

    ##2-d array test consistency
#    arr = numpy.array([[6, 4, 3, 7], [0, 3, 3, 5], [2, 8, 9, 3]]) #2d numpy array
#    result = isotonic_l1(arr, var_type="CONTINUOUS")

    ##2-d array test consistency, constraints
#    arr = numpy.array([[6, 4, 3, 7], [0, 3, 3, 5], [2, 8, 9, 3]])
#    result = isotonic_l1(arr, var_type="CONTINUOUS", starts_at=0, ends_at=[8, 5, 3], weighted_total=[12, 7, 5])

#    arr = numpy.array([0, -1, 1, 3, 5, 6, 0, 0])
#    result = isotonic_l2(arr, var_type="CONTINUOUS", cumsum=False, starts_at=0, ends_at=2, weighted_total=11)
#    print('result: ', result)
    #[  0.00000000e+00   2.12114104e-10   1.00000000e+00   2.00000000e+00 2.00000000e+00   2.00000000e+00   2.00000000e+00   2.00000000e+00]

    arr = [0, -1, 1, 3, 5, 6, 0, 0]
