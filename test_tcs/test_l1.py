#yzk5145@mlltrng01:~/optimization/test_tcs$ pytest

import numpy

import isotonic as iso

def test_nonnegative():
    arr = [6, 4, 3, 7]
    result = iso.isotonic_l1(arr)
    assert numpy.array_equal(result, numpy.array([4.0, 4.0, 4.0, 7.0]))

    result = iso.isotonic_l1(arr, var_type="CONTINUOUS")
    assert numpy.array_equal(result, numpy.array([4.0, 4.0, 4.0, 7.0]))


def test_minimize():

    arr = [2, 3, 5]
    result = iso.isotonic_l1(arr)
    assert numpy.array_equal(result, numpy.array([2.0, 3.0, 5.0]))
    assert numpy.array_equal(result, numpy.array([2, 3, 5]))

    result = iso.isotonic_l1(arr, var_type="CONTINUOUS")
    assert numpy.array_equal(result, numpy.array([2.0, 3.0, 5.0]))


def test_total_popul():
    arr =  [6, 4, 3, 7]
    result = iso.isotonic_l1(arr, weighted_total=11)
    assert numpy.array_equal(result, numpy.array([2.0, 4.0, 4.0, 7.0])) #obj = 5

    result = iso.isotonic_l1(arr, var_type="CONTINUOUS", weighted_total=11)
    assert numpy.array_equal(result, numpy.array([3.3333333333333335, 3.3333333333333335, 3.3333333333333335, 7.0])) #3.666666667e+00

    arr = [0, 3, 3, 5]
    result = iso.isotonic_l1(arr, weighted_total=9)
    assert numpy.array_equal(result, numpy.array([0.0, 3.0, 3.0, 5.0])) #obj = 0

    result = iso.isotonic_l1(arr, var_type="CONTINUOUS", weighted_total=9)
    assert numpy.array_equal(result, numpy.array([0.0, 3.0, 3.0, 5.0]))

def test_x0():
    arr =  [6, 4, 3, 7]
    result = iso.isotonic_l1(arr, starts_at=0, weighted_total=11)
    assert (numpy.array_equal(result, numpy.array([0.0, 3.0, 4.0, 6.0])) or numpy.array_equal(result, numpy.array([0.0, 5.0, 5.0, 7.0]))) #obj = 9

    result = iso.isotonic_l1(arr, var_type="CONTINUOUS", starts_at=0, weighted_total=11)
    assert numpy.array_equal(result, numpy.array([0.0, 4.0, 4.0, 6.333333333333333])) #obj= 7.666666667e+00  #3*6.333333333333333=19


    result = iso.isotonic_l1(arr, starts_at=1, weighted_total=11)
    assert (numpy.array_equal(result, numpy.array([1.0, 3.0, 3.0, 6.0])) or numpy.array_equal(result, numpy.array([1.0, 4.0, 5.0, 7.0]))) #obj = 7

    result = iso.isotonic_l1(arr, var_type="CONTINUOUS", starts_at=1, weighted_total=11)
    assert numpy.array_equal(result, numpy.array([1.0, 4.0, 4.0, 6.666666666666667])) #obj= 6.333333333e+00 #3*6.666666666666667=20

def test_xend():
    arr = [6, 4, 3, 7]
    result = iso.isotonic_l1(arr, starts_at=0, ends_at=5, weighted_total=11)
    assert numpy.array_equal(result, numpy.array([0.0, 2.0, 2.0, 5.0]))

    result = iso.isotonic_l1(arr, var_type="CONTINUOUS", starts_at=0, ends_at=5, weighted_total=11) #
    assert numpy.array_equal(result, numpy.array([0.0, 2.0, 2.0, 5.0])) #obj=1.100000000e+01


    result = iso.isotonic_l1(arr, starts_at=0, ends_at=5)
    assert (numpy.array_equal(result, numpy.array([0.0, 4.0, 4.0, 5.0])) or numpy.array_equal(result, numpy.array([0.0, 3.0, 3.0, 5.0])))

    result = iso.isotonic_l1(arr, var_type="CONTINUOUS", starts_at=0, ends_at=5)
    assert (numpy.array_equal(result, numpy.array([0.0, 4.0, 4.0, 5.0])) or numpy.array_equal(result, numpy.array([0.0, 3.0, 3.0, 5.0]))) #obj = 9.000000000e+00


    result = iso.isotonic_l1(arr, ends_at=100)
    assert numpy.array_equal(result, numpy.array([4.0, 4.0, 4.0, 100.0]))

    result = iso.isotonic_l1(arr, var_type="CONTINUOUS", ends_at=100)
    assert numpy.array_equal(result, numpy.array([4.0, 4.0, 4.0, 100.0])) #obj=9.600000000e+01

def test_joint_allconstrs():
    arr = numpy.array([[6, 4, 3, 7], [0, 3, 3, 5], [2, 8, 9, 3]])
    result = iso.isotonic_l1(arr, var_type="CONTINUOUS", starts_at=0, ends_at=[8, 5, 3], weighted_total=[12, 7, 5])
    assert numpy.array_equal(result, numpy.array([ [0.0, 6.0, 6.0, 8.0],
                                                    [0.0, 4.0, 4.0, 5.0],
                                                    [0.0, 2.0, 2.0, 3.0]]))

def test_household_size():
    arr = [0, -1, 1, 3, 5, 6, 0, 0]
    result = iso.isotonic_l1(arr, var_type="CONTINUOUS", cumsum=False, starts_at=0, ends_at=2, weighted_total=11)
    assert numpy.array_equal(result, numpy.array([0., 0., 1., 2., 2., 2., 2., 2.])) #obj=13

    result = iso.isotonic_l1(arr, var_type="CONTINUOUS", cumsum=False, starts_at=1, ends_at=2, weighted_total=11)
    assert numpy.array_equal(result, numpy.array([1., 1., 1., 1.5, 1.5, 1.5, 1.5, 2.])) #obj=16


def test_input_format():
    arr = numpy.array([0, -1, 1, 3, 5, 6, 0, 0])
    result = iso.isotonic_l1(arr, var_type="CONTINUOUS", cumsum=False, starts_at=0, ends_at=2, weighted_total=11)
    assert numpy.array_equal(result, numpy.array([0., 0., 1., 2., 2., 2., 2., 2.])) #obj=13

    result = iso.isotonic_l1(arr, var_type="CONTINUOUS", cumsum=False, starts_at=1, ends_at=2, weighted_total=11)
    assert numpy.array_equal(result, numpy.array([1., 1., 1., 1.5, 1.5, 1.5, 1.5, 2.])) #obj=16
