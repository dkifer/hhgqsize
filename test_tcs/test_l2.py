#yzk5145@mlltrng01:~/optimization/test_tcs$ pytest
import numpy
import isotonic as iso

#how to fix the floating point problem, different software?

def test_nonnegative():
    arr = [6, 4, 3, 7]
    result = iso.isotonic_l2(arr)
    assert numpy.array_equal(result, numpy.array([4.0, 4.0, 4.0, 7.0])) #obj=5.000000000000e+00

    result = iso.isotonic_l2(arr, var_type="CONTINUOUS")
    assert numpy.array_equal(result, numpy.array([4.333333333324241, 4.333333333329764, 4.3333333333468005, 7.000000000000168])) #obj=4.66666667e+00

    arr = [6, 4, 3]
    result = iso.isotonic_l2(arr)
    assert numpy.array_equal(result, numpy.array([4.0, 4.0, 4.0]))

    result = iso.isotonic_l2(arr, var_type="CONTINUOUS")
    assert numpy.array_equal(result, numpy.array([4.333333333323273, 4.3333333333310895, 4.3333333333468795]))

def test_total_popul():
    arr =  [6, 4, 3, 7]
    result = iso.isotonic_l2(arr, weighted_total=11)
    assert numpy.array_equal(result, numpy.array([4.0, 4.0, 5.0, 8.0]))

    result = iso.isotonic_l2(arr, var_type="CONTINUOUS", weighted_total=11)
    assert numpy.array_equal(result, numpy.array([4.083333333320554, 4.083333333383475, 4.083333333462029, 7.750000000055353])) #obj=5.41666667e+00

    arr = [0, 3, 3, 5]
    result = iso.isotonic_l2(arr, weighted_total=9)
    assert numpy.array_equal(result, numpy.array([0.0, 3.0, 3.0, 5.0]))

    result = iso.isotonic_l2(arr, var_type="CONTINUOUS", weighted_total=9)
    assert numpy.array_equal(result, numpy.array([5.197813544957489e-05, 2.9999803532767735, 3.0000101961976515, 5.000014175869958])) #obj=3.39263803e-09

def test_x0():
    arr =  [6, 4, 3, 7]
    result = iso.isotonic_l2(arr, starts_at=0, weighted_total=11)
    assert numpy.array_equal(result, numpy.array([0.0, 3.0, 4.0, 6.0]))

    result = iso.isotonic_l2(arr, var_type="CONTINUOUS", starts_at=0, weighted_total=11)
    assert numpy.array_equal(result, numpy.array([0.0, 3.7727272644179206, 3.772727281348431, 6.181818181922117])) #obj=3.73181818e+01

    result = iso.isotonic_l2(arr, starts_at=1, weighted_total=11)
    assert numpy.array_equal(result, numpy.array([1.0, 3.0, 3.0, 6.0])) #obj=27

    result = iso.isotonic_l2(arr, var_type="CONTINUOUS", starts_at=1, weighted_total=11)
    assert numpy.array_equal(result, numpy.array([1.0, 3.681818181679767, 3.6818181819613884, 6.454545454547052])) #obj=2.58636364e+01

def test_xend():
    arr = [6, 4, 3, 7]
    result = iso.isotonic_l2(arr, starts_at=0, ends_at=5, weighted_total=11)
    assert numpy.array_equal(result, numpy.array([0.0, 2.0, 2.0, 5.0])) #obj=45

    result = iso.isotonic_l2(arr, var_type="CONTINUOUS", starts_at=0, ends_at=5, weighted_total=11)
    assert numpy.array_equal(result, numpy.array([0.0, 1.999999995465409, 2.0000000045345905, 5.0])) #obj=45

    result = iso.isotonic_l2(arr, starts_at=0, ends_at=5)
    assert (numpy.array_equal(result, numpy.array([0.0, 4.0, 4.0, 5.0])) or numpy.array_equal(result, numpy.array([0.0, 3.0, 3.0, 5.0]))) #obj=41

    result = iso.isotonic_l2(arr, var_type="CONTINUOUS", starts_at=0, ends_at=5)
    assert numpy.array_equal(result, numpy.array([0.0, 3.4999999952169065, 3.5000000048272617, 5.0])) #obj=40.5


    result = iso.isotonic_l2(arr, ends_at=100)
    assert numpy.array_equal(result, numpy.array([4.0, 4.0, 4.0, 100.0])) #8654

    result = iso.isotonic_l2(arr, var_type="CONTINUOUS", ends_at=100)
    assert numpy.array_equal(result, numpy.array([4.333333333304801, 4.333333333345908, 4.333333333357729, 100.0])) #obj=8.65366667e+03


""" to do floating point
def test_household_size():

    arr = [0, -1, 1, 3, 5, 6, 0, 0]
    result = iso.isotonic_l2(arr, var_type="CONTINUOUS", cumsum=False, starts_at=0, ends_at=2, weighted_total=11)
    assert (result[1]-2.12114104e-10)*(result[1]-2.12114104e-10) < 1
    assert (result[0]-0.0)*(result[0]-0.0) < 1
    assert (result[2]-1.0)*(result[2]-1.0) < 1
    assert (result[3]-2.0)*(result[3]-2.0) < 1
    assert (result[4]-2.0)*(result[4]-2.0) < 1
    assert (result[5]-2.0)*(result[5]-2.0) < 1
    assert (result[6]-2.0)*(result[6]-2.0) < 1
    assert (result[7]-2.0)*(result[7]-2.0) < 1
    #assert numpy.array_equal(result, numpy.array([0.0, 2.12114104e-10, 1.0, 2.0, 2.0, 2.0, 2.0, 2.0]))
"""
