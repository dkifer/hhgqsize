import numpy
import collections

Bootstrap = collections.namedtuple('Bootstrap', ['name', 'sizehist'])

#data source for household sizes:
#https://factfinder.census.gov/faces/tableservices/jsf/pages/productview.xhtml?pid=DEC_10_SF1_H13&prodType=table
national = Bootstrap('usa', numpy.array([0, 31204909, 38242628, 18757985, 15625246, 7538631, 3074699]))

states = [
    Bootstrap('al', numpy.array([0, 1883791, 516696,  632291,  322941,  245326,  106771,  37510,   22256])),
    Bootstrap('ak', numpy.array([0, 258058,  66073,   83852,   41891,   34124,   18122,   7546,    6450])),
    Bootstrap('az', numpy.array([0, 2380990, 621008,  814955,  344326,  296655,  164944,  77095,   62007])),
    Bootstrap('ar', numpy.array([0, 1147084, 310792,  402111,  186450,  142819,  65311,   24496,   15105])),
    Bootstrap('ca', numpy.array([0, 12577498,    2929442, 3653802, 2043812, 1883451, 1040408, 507471,  519112])),
    Bootstrap('co', numpy.array([0, 1972868, 550794,  673027,  301196,  256692,  115994,  45650,   29515])),
    Bootstrap('ct', numpy.array([0, 1371087, 373648,  443095,  226658,  197116,  84916,   29348,   16306])),
    Bootstrap('de', numpy.array([0, 342297,  87496,   117500,  56966,   45778,   20945,   8164,    5448])),
    Bootstrap('dc', numpy.array([0, 266707,  117431,  77845,   32679,   20274,   9913,    4572,    3993])),
    Bootstrap('fl', numpy.array([0, 7420802, 2021781, 2643162, 1152686, 900377,  419230,  168395,  115171])),
    Bootstrap('ga', numpy.array([0, 3585584, 909474,  1130138, 620894,  515518,  244075,  98120,   67365])),
    Bootstrap('hi', numpy.array([0, 455338,  106175,  139162,  76348,   60942,  33338,   17541,   21832])),
    Bootstrap('id', numpy.array([0, 579408,  137785,  204131,  87542,   75327,   41323,   20622,   12678])),
    Bootstrap('il', numpy.array([0, 4836972, 1346312, 1509908, 756467,  659731,  330647,  134192,  99715])),
    Bootstrap('in', numpy.array([0, 2502154, 671920,  854193,  398466,  326993,  155435,  59151,   35996])),
    Bootstrap('ia', numpy.array([0, 1221576, 347479,  445876,  173412,  147285,  69530,   24579,   13415])),
    Bootstrap('ks', numpy.array([0, 1112096, 309338,  382396,  166121,  140905,  70093,   27289,   15954])),
    Bootstrap('ky', numpy.array([0, 1719965, 473447,  591547,  290441,  219764,  93848,   32726,   18192])),
    Bootstrap('la', numpy.array([0, 1728360, 464334,  553276,  301380,  231047,  110627,  41476,   26220])),
    Bootstrap('me', numpy.array([0, 557219,  159533,  213695,  84340,   64010,   23840,   7854,    3947])),
    Bootstrap('md', numpy.array([0, 2156411, 563003,  679538,  368919,  303341,  141659,  57319,   42632])),
    Bootstrap('ma', numpy.array([0, 2547075, 732263,  813166,  417216,  353676,  150842,  51409,   28503])),
    Bootstrap('mi', numpy.array([0, 3872508, 1079678, 1307449, 603482,  504315,  234074,  88972,   54538])),
    Bootstrap('mn', numpy.array([0, 2087227, 584008,  724386,  307794,  274621,  123002,  44258,   29158])),
    Bootstrap('ms', numpy.array([0, 1115768, 293807,  356795,  194682,  150650,  72933,   27883,   19018])),
    Bootstrap('mo', numpy.array([0, 2375611, 672276,  822516,  370462,  296548,  134977,  49861,   28971])),
    Bootstrap('mt', numpy.array([0, 409607,  121775,  153975,  57046,   44407,   20466,   7563,    4375])),
    Bootstrap('ne', numpy.array([0, 721130,  206807,  251160,  103607,  87977,   44352,   17207,   10020])),
    Bootstrap('nv', numpy.array([0, 1006250, 258409,  332747,  157436,  128617,  70709,   32758,   25574])),
    Bootstrap('nh', numpy.array([0, 518973,  133057,  188923,  85046,   70835,   27365,   9286,    4461])),
    Bootstrap('nj', numpy.array([0, 3214360, 811221,  957682,  558029,  506107,  231727,  87444,   62150])),
    Bootstrap('nm', numpy.array([0, 791395,  221347,  260244,  121352,  98041,   52997,   21845,   15569])),
    Bootstrap('ny', numpy.array([0, 7317755, 2130670, 2215770, 1176503, 969467,  470263,  192977,  162105])),
    Bootstrap('nc', numpy.array([0, 3745155, 1011348, 1287934, 627435,  486969,  209317,  76642,   45510])),
    Bootstrap('nd', numpy.array([0, 281192,  88563,   102531,  38513,   30558,   13982,   4700,    2345])),
    Bootstrap('oh', numpy.array([0, 4603435, 1328550, 1568090, 720134,  575012,  262007,  95599,   54043])),
    Bootstrap('ok', numpy.array([0, 1460450, 401153,  502657,  229958,  182567,  88893,   34343,   20879])),
    Bootstrap('or', numpy.array([0, 1518938, 416747,  548344,  229273,  182750,  83789,   34329,   23706])),
    Bootstrap('pa', numpy.array([0, 5018904, 1433415, 1705503, 804124,  637442,  276410,  99988,   62022])),
    Bootstrap('ri', numpy.array([0, 413600,  122488,  133513,  67339,   54010,   23314,   8232,    4704])),
    Bootstrap('sc', numpy.array([0, 1801181, 477894,  623419,  304098,  231903,  103014,  37833,   23020])),
    Bootstrap('sd', numpy.array([0, 322282,  94638,   115675,  44573,   37271,   18648,   7013,    4464])),
    Bootstrap('tn', numpy.array([0, 2493552, 671286,  859346,  419516,  318385,  139975,  52038,   33006])),
    Bootstrap('tx', numpy.array([0, 8922933, 2163266, 2703417, 1482329, 1312995, 716376,  310605,  233945])),
    Bootstrap('ut', numpy.array([0, 877692,  164018,  256969,  140050,  132454,  90201,   54189,   39811])),
    Bootstrap('vt', numpy.array([0, 256442,  72233,   96889,   39695,   31210,   11107,   3480,    1828])),
    Bootstrap('va', numpy.array([0, 3056058, 795117,  1024641, 520503,  420101,  182216,  69216,   44264])),
    Bootstrap('wa', numpy.array([0, 2620076, 711619,  904232,  406397,  338260,  151893,  62772,   44903])),
    Bootstrap('wv', numpy.array([0, 763831,  217308,  278337,  124879,  89437,   35982,   11812,   6076])),
    Bootstrap('wi', numpy.array([0, 2279768, 642507,  817250,  339536,  284532,  124387,  44504,   27052])),
    Bootstrap('wy', numpy.array([0, 226879,  63480,   83568,   33043,   26654,   12444,   4825,    2865])),
    Bootstrap('pr', numpy.array([0, 1376531, 327560,  391452,  280818,  227988,  99560,   31434,   17719])),
]

def synthdata(seed=23, secret_max_size=11000, public_max_size=100000, num_outliers = 50, boot=national):
    #this function will randomly generate data
    """
        produces 1 histogram to simulate the U.S. group population.
        The result is a histogram h with h[i] being the number of groups with i people in it.
    """
    #The parameter you pass to numpy.random.RandomState is the seed for the generator, which
    #specifies the starting point for a sequence of pseudorandom numbers. If you seed two different
    #generators with the same thing, you will get the same sequence of results.
    prng = numpy.random.RandomState(seed)

    sizehist = numpy.zeros(public_max_size + 1, dtype=numpy.int32)
    #print "len(sizehist): ", len(sizehist) #len(sizehist):  100001
    secret_max_size = min(secret_max_size, public_max_size)

    #data source for household sizes:
    #https://factfinder.census.gov/faces/tableservices/jsf/pages/productview.xhtml?pid=DEC_10_SF1_H13&prodType=table

    sizehist[0: boot.sizehist.size] = boot.sizehist

    ratio = min(0.7, sizehist[boot.sizehist.size-1]/float(sizehist[boot.sizehist.size - 2]))
    index = boot.sizehist.size-1

    while sizehist[index] > 0:
        newcount = prng.binomial(n=sizehist[index], p=ratio)
        index = index + 1
        sizehist[index]=newcount
    outliers = prng.randint(low=1, high=secret_max_size+1, size=num_outliers)

    for size in outliers:
        sizehist[size] += 1

    return sizehist

def statedata(seed=23, secret_max_size=11000, public_max_size=100000, num_outliers = 50, boot=states):
    """
        produces a dictionary of size histograms where the key is a state and
        the value is a group size histogram. note that the state histograms
        are not necessarily consistent with the histogram you would get from synthdata(seed)
    """
    result = {}

    for st in states:
        seed = seed + 1
        result[st.name] = synthdata(seed, secret_max_size, public_max_size, num_outliers, st)
    #print("result['va'][:15]: ", result['va'][:15])
    #[      0 3056058  795117 1024641  520503  420101  182216   69216   44264
    #28335   18073   11530    7349    4657    3022]
    #print("result['pa'][:15]: ", result['pa'][:15])
    #[      0 5018904 1433415 1705503  804124  637442  276410   99988   62022
    #38589   23954   14770    9099    5706    3474]
    #print("len(result['va']): ", len(result['va'])) #len(result['va']):  100001
    #print('len(result): ', len(result)) #len(result):  52
    return result

if __name__ == '__main__':
    """
    a1 = synthdata()
    a2 = synthdata()
    a3 = numpy.copy(a1)
    #print numpy.array_equal(a1, a2) #False, so every time it calls synthdata(), it generate different data
    print (numpy.array_equal(a1, a3))
    """
    """
    #after replacing numpy.random() to prng
    a1 = synthdata()
    a2 = synthdata()
    print (numpy.array_equal(a1, a2)) #True, so same seed, same output.
    """
#    synthdata()
#    statedata()
#    statedata(seed=24)
    #different generate different data
