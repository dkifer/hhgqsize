import numpy

def get(arr, i, default=0):
    return default if i >= numpy.size(arr) else arr[i]

def index_max_positive(a):
    last_index = 0
    for (i, value) in enumerate(a):
        if value > 0:
            last_index = i
    return last_index

def compatibility_resize(a, b):
    len_a = numpy.size(a)
    len_b = numpy.size(b)
    #print ("len_a: ", len_a) #('len_a: ', 4)
    #print ("len_b: ", len_b) #('len_b: ', 3)
    newa = a.copy()
    newb = b.copy()
    if len_a > len_b:
        newb.resize(len_a)
        #print ("newb: ", newb) #('newb: ', array([2, 2, 4, 0]))
    elif len_b > len_a:
        newa.resize(len_b)
    return newa, newb


def l1_error(a, b):
    newa, newb = compatibility_resize(a,b)
    answer = numpy.abs(newa-newb).sum()
    return float(answer)

def l2_error(a, b):
    newa, newb = compatibility_resize(a, b)
    answer = numpy.sqrt(((newa-newb)**2).sum())
    return float(answer)

def index_weighted_l1_error(a, b):
    newa, newb = compatibility_resize(a,b)
    answer = (numpy.abs(newa-newb) * numpy.arange(0, newa.size)).sum()
    return float(answer)

def max_element_error(a, b):
    return float(abs(index_max_positive(a) - index_max_positive(b)))

def num_groups_error(correct, estimated):
    sparse_a = (correct != 0).sum()
    sparse_b = (estimated != 0).sum()
    return float(abs(sparse_a - sparse_b)) #because numpy.int64 are not json serializable

def group_existence_error(correct, estimated):
    true_groups = set(numpy.where(correct != 0)[0])
    est_groups = set(numpy.where(estimated != 0)[0])
    return float(len(true_groups.symmetric_difference(est_groups)))

def prefix_l1_error(correct, estimated, starting_index=0, relative=False):
    newa, newb = compatibility_resize(correct,estimated)
    ca = newa[starting_index:].cumsum()
    cb = newb[starting_index:].cumsum()
    denominator = float(newa.sum()) if relative else 1.0
    return float(numpy.abs(ca - cb).sum()/denominator)

def prefix_l2_error(correct, estimated, starting_index=0, relative=False):
    newa, newb = compatibility_resize(correct,estimated)
    ca = newa[starting_index:].cumsum()
    cb = newb[starting_index:].cumsum()
    denominator = float(newa.sum()) if relative else 1.0
    return float(numpy.power(ca - cb, 2).sum()/denominator)


def emd(correct, estimated):
    newa, newb = compatibility_resize(correct, estimated)
#    print("hist newa:  ", newa)
#    print("hist newb: ", newb)

    diff = newa.sum() - newb.sum()
#    print("diff in emd: ", diff)

    #before replacing with diff, compute the error
    #cSube_err = 0
    #print("numpy.size(newa): ", numpy.size(newa))
    #for i in range(numpy.size(newa)):
    #      if newa[i]-newb[i] > 0:
    #        print("extra household in true: ", newa[i]-newb[i])
    #        print("i: ", i)
    #        cSube_err += i*abs(newa[i]-newb[i])
    #print("cSube_err: ", cSube_err)
    #
    #eSubc_err = 0
    #for i in range(numpy.size(newa)):
    #      if newb[i]-newa[i] > 0:
    #        print("extra household in inf: ", newb[i]-newa[i])
    #        print("i: ", i)
    #        eSubc_err += i*abs(newa[i]-newb[i])
    #print("eSubc_err: ", eSubc_err)



    if diff > 0:
        newb[0] = abs(diff)
        print("true total household > estimated")
    elif diff < 0:
        newa[0] = abs(diff)
        print("true total household FEWER estimated")




    ca = newa.cumsum()
    #print("cum sum ca: ", ca)
    cb = newb.cumsum()
    #print("cum sum cb: ", cb)
    #print("ca[-1]: ", ca[-1])
    #print("cb[-1]: ", cb[-1])
    #print("numpy.abs(ca -cb): ", numpy.abs(ca - cb))
    return float(numpy.abs(ca - cb).sum())

def emd_per_size(correct, estimated):
    top = emd(correct, estimated)
    bottom = (correct != 0).sum()
    return top/float(bottom)
"""
if __name__ == '__main__':
  #res = emd(numpy.array([0,1,2,4]), numpy.array([0,2,2,5]))
  #print ("res: ", res)
  #res = emd(numpy.array([0,0,0,0,0,1,0,0,0,0]), numpy.array([0,0,0,0,0,0,0,0,0,0]))
  #res = emd(numpy.array([0,0,0,0,0,0,0,0,0,0]), numpy.array([0,0,0,0,0,1,0,0,0,0]))
  #print("res: ", res)
  res = emd(numpy.array([0,0,0,0,0,2,0,0,0,1]), numpy.array([0,0,0,0,0,0,0,0,0,0])) #err_indices should be 19
"""
#def emd1(correct, estimated):
#    newa, newb = compatibility_resize(correct,estimated)
#    ia = 0
#    ib = 0
#    left_a = newa[ia]
    #left_b = newb[ib]
    #total = 0.0
    #moved = 0.0
    #while ia < newa.size and ib < newb.size:
       #if left_a == left_b:
           #moved = left_a * abs(ia - ib)
           ##print("{}({}) {}({}) {} {}".format(ia, ib, left_a, left_b, moved, total))
           #ia = ia + 1
           #ib = ib + 1
           #left_a = get(newa, ia)
           #left_b = get(newb, ib)
       #elif left_a > left_b:
           #moved = left_b * abs(ia - ib)
           ##print("{}({}) {}({}) {} {}".format(ia, ib, left_a, left_b, moved, total))
           #ib = ib + 1
           #left_a = left_a - left_b
           #left_b = get(newb, ib)
       #elif left_a < left_b:
           #moved = left_a * abs(ia - ib)
           ##print("{}({}) {}({}) {} {}".format(ia, ib, left_a, left_b, moved, total))
           ##ia = ia + 1
           #left_b = left_b - left_a
           #left_a = get(newa, ia)
       #total = total + moved
       #moved = 0.0
    #if left_a > 0:
        #moved = left_a * ia
        #ia = ia + 1
        #for i in range(ia, newa.size):
            #moved = moved + i * newa[i]
    #elif left_b > 0:
        #moved = left_b * ib
        #ib = ib + 1
        #for i in range(ib, newb.size):
            #moved = moved + i * newb[i]
    #total = total + moved
    #return float(total)
