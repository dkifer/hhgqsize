"""
python3 main.py <mode>
<mode> : nat, states, check
"""
import sys
#sys.path.append("./dkifer-hhgqsize/")
import os
import numpy
import time
import collections
import pandas as pd
from dask.distributed import Client

import synthdata as syn
import combine as com
import evaluate as eva

import isotonic as iso

res_dir = './mlltrng/'
if not os.path.exists(res_dir):
    os.mkdir(res_dir)

setups = []
"""
setups.append(('CONTnoConstr',
               {'var_type': 'CONTINUOUS', 'starts_at': None, 'ends_at': None, 'weighted_total': None}
              ))


setups.append(('CONTx0',
               {'var_type': 'CONTINUOUS', 'starts_at': 0, 'ends_at': None, 'weighted_total': None}
              ))


setups.append(('CONTx0xend',
               {'var_type': 'CONTINUOUS', 'starts_at': 0, 'ends_at': True, 'weighted_total': None}
              ))


setups.append(('CONTx0total',
               {'var_type': 'CONTINUOUS', 'starts_at': 0, 'ends_at': None, 'weighted_total': True}
              ))

setups.append(('CONTx0xendtotal',
               {'var_type': 'CONTINUOUS', 'starts_at': 0, 'ends_at': True, 'weighted_total': True}
              ))

"""

"""
setups.append(('INTnoConstr',
               {'var_type': 'INTEGER', 'starts_at': None, 'ends_at': None, 'weighted_total': None}
              ))


setups.append(('INTx0',
               {'var_type': 'INTEGER', 'starts_at': 0, 'ends_at': None, 'weighted_total': None}
              ))


setups.append(('INTx0xend',
               {'var_type': 'INTEGER', 'starts_at': 0, 'ends_at': True, 'weighted_total': None}
              ))


setups.append(('INTx0total',
               {'var_type': 'INTEGER', 'starts_at': 0, 'ends_at': None, 'weighted_total': True}
              ))
"""
setups.append(('INTx0xendtotal',
               {'var_type': 'INTEGER', 'starts_at': 0, 'ends_at': True, 'weighted_total': True}
              ))


def res_to_df(result, fn): #result is a list of dict
    df = pd.DataFrame()
    for row in result:

        df = df.append(row, ignore_index=True)

    df.to_csv(res_dir+'{0}.csv'.format(fn), index=False)


def to_hist(infer_cum_x):
    infer_hist_x = infer_cum_x[0]
    infer_hist_x = numpy.append(infer_hist_x, numpy.diff(infer_cum_x))
    return infer_hist_x

def run_synth_national(res_fn, seed, budget, public_max_size, var_type='CONTINUOUS', nonnegative=True,
        starts_at=None, ends_at=None, weighted_total=None):
#def run_synth_national(seed, budget, public_max_size, var_type='CONTINUOUS', nonnegative=True,
#        starts_at=None, ends_at=None, weighted_total=None):


    l1fn = res_dir+res_fn+'_l1estSubtrue'
    l2fn = res_dir+res_fn+'_l2estSubtrue'

    l1estfn = res_dir+res_fn+'_l1est'
    l2estfn = res_dir+res_fn+'_l2est'
    #print('l1fn: ', l1fn)
    #print('l2fn: ', l2fn)

    sizehist = syn.synthdata(seed=seed, public_max_size=public_max_size)

    #len(sizehist) is public_max_size +1
    if weighted_total is not None:
        true_total = (sizehist*numpy.arange(sizehist.size)).sum()
        weighted_total = true_total


    if ends_at is not None:
        sizecum = sizehist.cumsum()
        ends_at = sizecum[-1]

    if starts_at is not None:
        print ('starts_at constraint %s' %(starts_at))

    #not sure the max_size is public_max_size +1 or public_max_size
    prng = numpy.random.RandomState(seed)
    noisy_cum_sum = com.noisy_increasing_hist(sizehist, budget, prng, len(sizehist))

    start_time = time.time()
    l1_cum_x = iso.isotonic_l1(noisy_cum_sum, var_type=var_type,
        nonnegative=True, starts_at=starts_at, ends_at=ends_at, weighted_total=weighted_total)
    print("---L1 %s seconds ---" % (time.time() - start_time))



    l1_hist = to_hist(l1_cum_x)
    numpy.savetxt(l1estfn, l1_hist, delimiter=",")


    #print('l1_hist: ', l1_hist)
    #print('sizehist: ', sizehist)
    diff = l1_hist - sizehist
#    numpy.savetxt(l1fn, diff, delimiter=",")

    l1_eval = eva.emd(sizehist, l1_hist)

    start_time = time.time()
    l2_cum_x = iso.isotonic_l2(noisy_cum_sum, var_type=var_type,
        nonnegative=True, starts_at=starts_at, ends_at=ends_at, weighted_total=weighted_total)
    print("---L2 %s seconds ---" % (time.time() - start_time))



    l2_hist = to_hist(l2_cum_x)
    numpy.savetxt(l2estfn, l2_hist, delimiter=",")

    #print('l2_hist: ', l2_hist)
    #print('sizehist: ', sizehist)
    diff = l2_hist - sizehist
#    numpy.savetxt(l2fn, diff, delimiter=",")

    #Note that the input to emd is not an increasing histogram,
    #it is a histogram k where k[i] is the estimate of the number of groups of size i
    l2_eval = eva.emd(sizehist, l2_hist) #a number

    return {"seed": seed, "l1": l1_eval, "l2": l2_eval}


#def run_state(tmpf, st_k, st_sizehist, seed, budget, public_max_size, var_type='CONTINUOUS', nonnegative=True,
#        starts_at=None, ends_at=None, weighted_total=None):
def run_state(st_k, st_sizehist, seed, budget, public_max_size, var_type='CONTINUOUS', nonnegative=True,
        starts_at=None, ends_at=None, weighted_total=None):



    if weighted_total is not None:
        true_total = (st_sizehist*numpy.arange(st_sizehist.size)).sum()
        weighted_total = true_total


    if ends_at is not None:
        st_sizecum = st_sizehist.cumsum()
        assert st_sizehist.sum() == st_sizecum[-1]
        ends_at = st_sizecum[-1]

    if starts_at is not None:
        print ('starts_at constraint %s' %(starts_at))


    prng = numpy.random.RandomState(seed)
    noisy_cum_sum = com.noisy_increasing_hist(st_sizehist, budget, prng, len(st_sizehist))


    l1_cum_x = iso.isotonic_l1(noisy_cum_sum, var_type=var_type,
        nonnegative=True, starts_at=starts_at, ends_at=ends_at, weighted_total=weighted_total)

    if l1_cum_x is None:
        l1_eval = None

    else:
        l1_hist = to_hist(l1_cum_x)
        l1_eval = eva.emd(st_sizehist, l1_hist)



    l2_cum_x = iso.isotonic_l2(noisy_cum_sum, var_type=var_type,
        nonnegative=True, starts_at=starts_at, ends_at=ends_at, weighted_total=weighted_total)

    if l2_cum_x is None: #if l2_cum_x is None -> error
        l2_eval = None
    else:
        l2_hist = to_hist(l2_cum_x)
        l2_eval = eva.emd(st_sizehist, l2_hist) #a number


    return (st_k, {"seed": seed, "l1": l1_eval, "l2": l2_eval})

def sum_over_dic(states):


    allsts = numpy.zeros(len(states[list(states.keys())[0]]), dtype=numpy.int32)
    print('len(allsts): ', len(allsts)) #100001

    for k, v in states.items():
        assert len(states[k]) == len(states['pa'])

        allsts = allsts + v

    return allsts


def list_of_pair_to_dic(l):
    dic = collections.defaultdict(list)
    for key, value in l:
           dic[key].append(value)
    return dict(dic)

def evaluate_national():

    ####################
    ##Task1 - national##
    ####################
    client = Client()
    level = 'nat'
    for name, arg_dic in setups:
        jobs = []
        print('======name: ======', name)
        fn = 'setup-{0}_level-{1}'.format(name, level)

        for s in range(seed_start, seed_end+1):
            histfn = 'setup-{0}_level-{1}_seed-{2}'.format(name, level, s)
            jobs.append(client.submit(run_synth_national, histfn, s, budget, public_max_size,
                var_type=arg_dic['var_type'], starts_at=arg_dic['starts_at'],
                ends_at=arg_dic['ends_at'], weighted_total=arg_dic['weighted_total']))

        result = client.gather(jobs)
        print(result)

        res_to_df(result, fn)
    client.close()

def evaluate_states():

    ####################
    ##  Task2 - state ##
    ####################
    level = 'state'
    client = Client()
    for name, arg_dic in setups:
        print(name)

        jobs = []
        for s in range(seed_start, seed_end+1):
            #print("seed: ", s)
            states = syn.statedata(seed=s, public_max_size=public_max_size)

            #add the sum of these states as 'statesSum'
            allsts = sum_over_dic(states)
            states['statesSum'] = allsts
#            #parallelize
#            jobs = []
#            tmpf = open('tmp', 'w')
            for st_k, st_sizehist in states.items():
                print('a state synth: ',  st_k)


                #tmp = run_state(st_k, st_sizehist, s, budget, public_max_size,
                #    var_type=arg_dic['var_type'], starts_at=arg_dic['starts_at'],
                #    ends_at=arg_dic['ends_at'], weighted_total=arg_dic['weighted_total'])

                jobs.append(client.submit(run_state, st_k, st_sizehist, s, budget, public_max_size,
                    var_type=arg_dic['var_type'], starts_at=arg_dic['starts_at'],
                    ends_at=arg_dic['ends_at'], weighted_total=arg_dic['weighted_total']))

                #run_state(tmpf, st_k, st_sizehist, s, budget, public_max_size,
                #    var_type=arg_dic['var_type'], starts_at=arg_dic['starts_at'],
                #    ends_at=arg_dic['ends_at'], weighted_total=arg_dic['weighted_total'])
#                run_state(st_k, st_sizehist, s, budget, public_max_size,
#                    var_type=arg_dic['var_type'], starts_at=arg_dic['starts_at'],
#                    ends_at=arg_dic['ends_at'], weighted_total=arg_dic['weighted_total'])

        result = client.gather(jobs)

        merge_result = list_of_pair_to_dic(result)

        for s_id, rows in merge_result.items():
            fn = 'setup-{0}_level-{1}_st-{2}'.format(name, level, s_id)
            res_to_df(rows, fn)
    client.close()

def check():
    ########################################
    ##  Task3 - nat l1 very large error   ##
    ########################################
    client = Client()
    jobs = []
    level = 'nat'
    print("setups[0]: ", setups[0])
    for name, arg_dic in setups:
        print('======name: ======', name)
        if name == 'CONTnoConstr':
        #if name == 'CONTx0':
            specific_s = 47#26#22#50#49#48#40#34#30#24#21#45#25#22#23#22#21
            fn = 'setup-{0}_level-{1}_seed-{2}'.format(name, level, specific_s)
            jobs.append(client.submit(run_synth_national, fn, specific_s, budget, public_max_size,
                var_type=arg_dic['var_type'], starts_at=arg_dic['starts_at'],
                ends_at=arg_dic['ends_at'], weighted_total=arg_dic['weighted_total']))

    result = client.gather(jobs)
    print(result)

def gen_key_idx(sizehist_dic):
    """
    The statesSum is fixed as index 0. Sort other states key
    """
    #idx_table = []
    sorted_k = sorted(sizehist_dic.keys())
    sorted_k.remove('statesSum')
    sorted_k.insert(0, 'statesSum')
    print('fix statesSum index as 0, sorted_k: ', sorted_k) # ['statesSum', 'ak', 'al', 'ar',
    return sorted_k

def run_joint_model(res_fn, sizehist_dic, seed, budget, public_max_size, var_type='CONTINUOUS', nonnegative=True,
        starts_at=None, ends_at=None, weighted_total=None):

    """
    Inputs:
        sizehist_dic: a dictionary of size histograms
                        (where statesSum is the sum of all states hist)

    Notes: Add noise to the statesSum and each one of the states (privacy budge doubled)

    Enforce consistency:
        weighted_total (i.e., total popul) of statesSum = sum_i(weighted_total at state i)
        ends_at (i.e., total # household) of statesSum = sum_i(ends_at at state i)
    """

    l1estfn = res_dir+res_fn+'_l1est'


    noisy_cum_matrix = numpy.zeros((len(sizehist_dic), len(list(sizehist_dic.values())[0])))
    print('noisy_cum_matrix.shape: ', noisy_cum_matrix.shape) #(53, public_max_size+1)

    idx_table = gen_key_idx(sizehist_dic)

    prng = numpy.random.RandomState(seed)
    for k, sizehist in sizehist_dic.items():
        noisy_cum_sum = com.noisy_increasing_hist(sizehist, budget, prng, len(sizehist))
        st_idx = idx_table.index(k)
        noisy_cum_matrix[st_idx] = noisy_cum_sum

    print('noisy_cum_matrix: ', noisy_cum_matrix)

    if starts_at is not None:
        print ('starts_at constraint %s' %(starts_at))

    if ends_at is not None:
        #ends_at should be a list of length len(sizehist_dic)
        ends_at = [None]*len(sizehist_dic)
        for k, sizehist in sizehist_dic.items():
            sizecum = sizehist.cumsum()
            ends_at[idx_table.index(k)] = sizecum[-1]

        assert ends_at[0] == sum(ends_at[1:])


    if weighted_total is not None:
        weighted_total = [None]*len(sizehist_dic)
        for k, sizehist in sizehist_dic.items():
            true_total = (sizehist*numpy.arange(sizehist.size)).sum()
            print(true_total)
            weighted_total[idx_table.index(k)] = true_total

        assert weighted_total[0] == sum(weighted_total[1:])


    start_time = time.time()
    l1_cum_x_matrix = iso.isotonic_l1(noisy_cum_matrix, var_type=var_type,
        nonnegative=True, starts_at=starts_at, ends_at=ends_at, weighted_total=weighted_total)
    print("---L1 %s seconds ---" % (time.time() - start_time))
    print('l1_cum_x_matrix: ', l1_cum_x_matrix)

    numpy.savetxt(l1estfn, l1_cum_x_matrix)


    #evaluate_joint_matrix()




def run_nat_household(res_fn, seed, budget, public_max_size, var_type="INTEGER",
        cumsum=False, nonnegative=True, starts_at=None, ends_at=None, weighted_total=None):

    l1estfn = res_dir+res_fn+'_l1est'
    l2estfn = res_dir+res_fn+'_l2est'

    sizehist = syn.synthdata(seed=seed, public_max_size=public_max_size)
#    sizehist = numpy.array([0, 5, 3])
#    sizehist = numpy.array([0, 5, 3, 1, 2])

#    print('sizehist: ', sizehist)

    if starts_at is not None:
        # the first non-zero element's index from the sizehist
        nonzero_ids = numpy.nonzero(sizehist)[0] #[0] specify the row idx
        first_hhsize = nonzero_ids[0]
        print('first_hhsize: ', first_hhsize)
        starts_at = first_hhsize

    if ends_at is not None:
        # max household size's
        # i.e., the last non-zero index from the sizehist
        nonzero_ids = numpy.nonzero(sizehist)[0]
        #print('nonzero_ids: ', nonzero_ids)
        max_hhsize = nonzero_ids[nonzero_ids.size-1]
        ends_at = max_hhsize
        print('ends_at: ', ends_at)


    if weighted_total is not None:
        # total popul
        true_total = (sizehist*numpy.arange(sizehist.size)).sum()
        weighted_total = true_total
        print('true_total: ', true_total)


    prng = numpy.random.RandomState(seed)
    _, noisy_househist = com.noisy_group_sizes(sizehist, budget, prng)
    #print('noisy_househist: ', noisy_househist)
    start_time = time.time()
    l1_househist_x = iso.isotonic_l1(noisy_househist, var_type=var_type, cumsum=cumsum,
        nonnegative=True, starts_at=starts_at, ends_at=ends_at, weighted_total=weighted_total)
    print("---L1 %s seconds ---" % (time.time() - start_time))

    numpy.savetxt(l1estfn, l1_househist_x, delimiter=",")
    #print('l1_househist_x: ', l1_househist_x)
    #[ 1.   1.   1.   1.5  1.5  1.5  1.5  2. ] -> how to convert to size household hist

    #[ 1.  1.  1.  1.  1.  2.  2.  2.]

    l1_sizehist = numpy.zeros(int(max(l1_househist_x))+1)
    for i in l1_househist_x:
        l1_sizehist[int(i)] += 1
    #print('l1_sizehist: ', l1_sizehist)

    l1_eval = eva.emd(sizehist, l1_sizehist)
    print('l1_eval: ', l1_eval)
    return {"seed": seed, "l1": l1_eval, "l2": None}




if __name__ == '__main__':
    seed_start = 21#20
    seed_end = 21#20#20#21#60 #60#23#60#40#50
    public_max_size = 100000#60#100000#60#100000#60#100#100000
    budget = 1.0
    mode = None#'nat'

    if len(sys.argv) > 1:
        mode = sys.argv[1]

    if mode == 'nat':
        #task 1
        evaluate_national()
    elif mode == 'state':
        #task 2
        evaluate_states()
    elif mode == 'check':
        check()
    else:
        print('unknown mode')

    """
    ##########################
    ##  Task4 - joint model ##
    ##########################
    level = 'joint'
    for name, arg_dic in setups:
        jobs = []
        print('======name: ======', name)

        for s in range(seed_start, seed_end+1):

            states = syn.statedata(seed=s, public_max_size=public_max_size)

            #add the sum of these states as 'statesSum'
            allsts = sum_over_dic(states)
            states['statesSum'] = allsts

            print('len(states): ', len(states))
            fn = 'setup-{0}_level-{1}_seed-{2}'.format(name, level, s)
            run_joint_model(fn, states, s, budget, public_max_size,
                var_type=arg_dic['var_type'], starts_at=arg_dic['starts_at'],
                ends_at=arg_dic['ends_at'], weighted_total=arg_dic['weighted_total'])

    """

    ############################
    ## Task5 - household size ##
    ############################

    client = Client()
    level = 'SizeNat'
#    for name, arg_dic in setups[5:]:
    for name, arg_dic in setups[0:]:
        jobs = []
        print('======name: ======', name)
        fn = 'setup-{0}_level-{1}'.format(name, level)

        for s in range(seed_start, seed_end+1):
            histfn = 'setup-{0}_level-{1}_seed-{2}'.format(name, level, s)

            #run_nat_household(histfn, s, budget, public_max_size,
            #    var_type=arg_dic['var_type'], starts_at=arg_dic['starts_at'],
            #    ends_at=arg_dic['ends_at'], weighted_total=arg_dic['weighted_total'])
            jobs.append(client.submit(run_nat_household, histfn, s, budget, public_max_size,
                var_type=arg_dic['var_type'], starts_at=arg_dic['starts_at'],
                ends_at=arg_dic['ends_at'], weighted_total=arg_dic['weighted_total']))

        result = client.gather(jobs)
        print(result)

        res_to_df(result, fn)
    client.close()



"""
seed 20 21 22 , one setup, 53 states, dask.dist
/export/anaconda/anaconda3/lib/python3.6/multiprocessing/semaphore_tracker.py:129: UserWarning: semaphore_tracker: There appear to be 5 leaked semaphores to clean up at shutdown
  len(cache))
"""
